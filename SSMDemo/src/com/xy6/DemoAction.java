package com.xy6;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.opensymphony.xwork2.ActionSupport;

public class DemoAction extends ActionSupport {
	public static final long serialVersionUID = 1L;

	@Autowired
	private DemoHelper demoHelper = new DemoHelper();
	@Autowired
	private DemoDao demoDao;

	public String execute() {

		System.out.println("---curren year:" + demoHelper.getCurYear());

		DemoVO demoVO = new DemoVO();
		demoVO.setCode(1);
		List<DemoVO> listDemoVO = demoDao.selectByCode(demoVO);
		for (int i = 0; i < listDemoVO.size(); i++) {
			System.out.println("---i:" + listDemoVO.get(i).toString());
		}
		return "success";
	}
}
