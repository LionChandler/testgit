package com.xy6;

import java.util.List;

import org.mybatis.spring.support.SqlSessionDaoSupport;

public class DemoDao extends SqlSessionDaoSupport {
	public List<DemoVO> selectByCode(DemoVO demoVO) {
		return getSqlSession().selectList("selectByCode", demoVO);
	}
}
