package com.xy6;

public class DemoVO {
	private static final long serialVersionUID = 1L;

	private int code;
	private String name;

	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String toString() {
		String strTemp = "";
		strTemp += "code:";
		strTemp += this.getCode();
		strTemp += "\n";
		strTemp += "name:";
		strTemp += this.getName();
		strTemp += "\n";
		return strTemp;
	}
}
